import processing.serial.*;
Serial myPort;
float center = 100.0;
float XXpre = 0.0;
float YYpre = 0.0;
float XX = 0.0;
float YY = 0.0;
float cX, cY;
int c_pre, r_pre;
int c_delta, r_delta;
float XXX = 0.0;
float YYY = 0.0;
float X = 0.0;
float Y = 0.0;
float X_pre = 0.0;
float Y_pre = 0.0;
float w = 0.0;
float h = 0.0;
float [] arrayX = new float[1024];
float [] arrayY = new float [1024];
float [] arrayScaleX= new float[1024];
float [] arrayScaleY=new float[1024];
float MaxX, MinX ;
float MaxY, MinY = 0.0;
int SizeImage = 14;
int SizeImageIn = 10;
float Z = 0.0;
float Distan = 0.0;
float Angle = 0.0;
float AnglePre = 0.0;
float AnglePreToShow = 0.0;
float totalDistan = 0.0;
float xp =center, yp= center;
float XXp =center, YYp= center;
float rAngle=0.0;
boolean onClean = false;
boolean onIMU = false;
boolean isPrinted = false;
boolean onWrite = false;
boolean haveData = false;
String Send;
int count = 0, cA= 0;
PFont f;
String Distance=" ";
HLine h1 = new HLine(1.0, center); 
HLine h2 = new HLine(1.0, center); 
byte[] nums;// = { 0, 34, 5, 127, 52};
byte[] nums_;
int id = 0;
void setup()
{
  //size(600, 500, P3D);
  size(200, 200, P2D );
  background(255);
  translate(0, height);
  scale(1, -1);
  stroke(0);
  strokeWeight(1);
  line(0, center, 1000, center);
  line(center, 0, center, 1000);
  // if you have only ONE serial port active
  //myPort = new Serial(this, Serial.list()[0], 115200); // if you have only ONE serial port active

  // if you know the serial port name
  myPort = new Serial(this, "COM3", 115200);                    // Windows

}

void draw()
{

  serialEvent();  // read and parse incoming serial message

  if (onIMU) {
    onIMU = false;
    background(255); // set background to white
  }
  if (onClean) {
   if(count > 36 ){
    String name ="dataset/1/" +id+".jpeg" ;
    save(name);
   id++;
   }
    onClean = false;
    count = 0;
  }
  ////laser
  //strokeWeight(4);
  //stroke(204, 102, 0);
  //point(X+center,-Y+center);
  //if((X!=X_pre) ||(Y!=Y_pre) ){
  //strokeWeight(4);
  //stroke(255);
  //point(X+center,-Y+center);
  //}
  X_pre = X;
  Y_pre =Y;
  
  //draw
  if(count > 1){
  strokeWeight(3);  // Default
  stroke(0);
  h1.update(X, -Y);
  } else if(count == 1){
    background(255); // set background to white
    h1.setnew(X, -Y);
  }
  if (isPrinted) {
    isPrinted =false;
    print(Send);
    println();
  }
}

void serialEvent()
{
  int newLine = 13; // new line character in ASCII
  String message;
  do {
    message = myPort.readStringUntil(newLine); // read from port until new line
    if (message != null) {
      String[] list = split(trim(message), " ");
      if (list.length >= 2 && list[0].equals("Co:")) {
        X = float(list[1]); // convert to float x
        Y = float(list[2]); // convert to float y
        if(count > 1){
        haveData=true;
        cA++;
        cX = X;
        cY = Y;
        if (cX > MaxX ) MaxX = cX;
        if (cX < MinX ) MinX = cX;
        if (cY > MaxY ) MaxY = cY;
        if (cY < MinY ) MinY = cY;
        } else if(count == 1) {
        MaxX = cX;
        MinX = cX;
        MaxY = cY;
        MinY = cY; 
        }
        //Z = float(list[3]); // convert to float z
        count++;
      } else if (list[0].equals("Clean")) {
        onClean = true;
      } 

      if (list.length >= 2 && list[0].equals("Po:")) {
        XXX = float(list[3]); // convert to float x
        YYY = float(list[4]); // convert to float y
        Distan = float(list[5]); // convert to float z
        Angle = float(list[7]); // convert to float z
        rAngle=Angle+AnglePre;
        AnglePreToShow = AnglePre;
        AnglePre = rAngle;
        XX=XXpre+Distan*sin(radians(rAngle));
        YY=YYpre+Distan*cos(radians(rAngle));
        XXpre = XX;
        YYpre = YY;
      } 

      if (list[0].equals("IMUOK")) {
        Send =  message;
        isPrinted = true;
        onIMU = true;
      }
      if (list[0].equals(">>>Send:")) {
        Send =  message;
        isPrinted = true;
      }
      if (list[0].equals("Connected")) {
        Send =  message;
        isPrinted = true;
      }
      if (list[0].equals("Characteristic")) {
        print(">>>Begin<<<");
      }
    }
  } while (message != null);
}

class HLine { 
  float ypos=0, xpos =0;
  float R, Center;
  HLine (float r, float c  ) {
    R=r;
    Center = c;
  } 
  void update(float x, float y) { 
    line(xpos+Center, ypos+Center, x+Center, y+Center); 
    ypos = y; 
    xpos = x;
  }
    void setnew(float x, float y) { 
    ypos = y; 
    xpos = x;
  }
  
} 
