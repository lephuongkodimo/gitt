import processing.serial.*;
Serial myPort;
float center = 200.0;
float XXpre = 0.0;
float YYpre = 0.0;
float XX = 0.0;
float YY = 0.0;
float cX, cY;
int c_pre, r_pre;
int c_delta, r_delta;
float XXX = 0.0;
float YYY = 0.0;
float X = 0.0;
float Y = 0.0;
float w = 0.0;
float h = 0.0;
float [] arrayX = new float[1024];
float [] arrayY = new float [1024];
float [] arrayScaleX= new float[1024];
float [] arrayScaleY=new float[1024];
float MaxX, MinX ;
float MaxY, MinY = 0.0;
int SizeImage = 14;
int SizeImageIn = 10;
float Z = 0.0;
float Distan = 0.0;
float Angle = 0.0;
float AnglePre = 0.0;
float AnglePreToShow = 0.0;
float totalDistan = 0.0;
float xp =center, yp= center;
float XXp =center, YYp= center;
float rAngle=0.0;
boolean onClean = false;
boolean onIMU = false;
boolean isPrinted = false;
boolean onWrite = false;
boolean haveData = false;
String Send;
int count = 0, cA= 0 ,id = 0;
PFont f;
String Distance=" ";
HLine h1 = new HLine(1.0, center); 
HLine h2 = new HLine(1.0, center); 
HLine h3 = new HLine(1.0, center); 
byte[] nums;// = { 0, 34, 5, 127, 52};
byte[] nums_;
void setup()
{
  //size(600, 500, P3D);
  size(400, 400, P2D );
  background(255);
  translate(0, height);
  scale(1, -1);
  stroke(0);
  strokeWeight(1);
  line(0, center, 1000, center);
  line(center, 0, center, 1000);
  stroke(255, 0, 0);
  f = createFont("Arial", 20, true);
  textFont(f, 20);
  // if you have only ONE serial port active
  //myPort = new Serial(this, Serial.list()[0], 115200); // if you have only ONE serial port active

  // if you know the serial port name
  myPort = new Serial(this, "COM3", 115200);                    // Windows
  //myPort = new Serial(this, "/dev/ttyACM0", 9600);             // Linux
  //myPort = new Serial(this, "/dev/cu.usbmodem1217321", 9600);  // Mac

  textSize(16); // set text size
  textMode(SHAPE); // set text mode to shape
  //initDraw();
  

  
  nums = new byte [196];
  nums_ = new byte [196];
  MaxX = cX;
  MinX = cX;
  MaxY = cY;
  MinY = cY;
}

void draw()
{

  serialEvent();  // read and parse incoming serial message
  //background(255); // set background to white
  if (haveData) {
    arrayX[cA-1] = cX;
    arrayY[cA-1] = cY;

    haveData = false;
  }//drawArduino();
  //popMatrix(); // end of object
  if (onClean) {
    onClean= false;
    if (cA > 36) {
      w = abs(MaxX-MinX);
      h = abs(MaxY-MinY);

      noFill();
      stroke(204, 102, 0);
      rect(MinX+center, -MaxY+center, w, h);
      if (w < 2 || h < 2)
      {
        print("You draw too short\n");
        count = 0;
        cA= 0;
        return;
      }
      for (int i = 0; i < cA; i ++)
      {
        arrayScaleX[i]=((arrayX[i]-MinX)/w)*SizeImageIn;
        arrayScaleY[i]=((MaxY-arrayY[i])/h)*SizeImageIn;
        //print("(x,y)=");print(arrayScaleX[i]);print("\t");print(arrayScaleY[i]);print("\n");
      }
      for (int row = 0; row < SizeImage; row ++) {//row
        for (int col = 0; col < SizeImage; col++) {//col
          nums[col+SizeImage*row] =  (byte)(0) ;//0-255
          //print("nums["+row+SizeImage*col+"]=0");print("\n");
        }
      }

      c_pre=int(arrayScaleX[0])+2;
      r_pre=int (arrayScaleY[0])+2;
      for (int i = 1; i < cA; i ++) {
        int c = int(arrayScaleX[i])+2;
        int r = int (arrayScaleY[i])+2;
        
        
        c_delta = c - c_pre ;
        r_delta= r-r_pre;
        if ((c_delta) == 0 && (r_delta ==0)) {
          nums[c+r*SizeImage]= (byte) 255;
        } else
          if (c_delta ==0) {
            for ( int j = 0; j < abs(r_delta); j++ ) {
              int c_new = c_pre;
              int r_new = r_pre + j*int((abs(r_delta)/(r_delta)));
              nums[c_new+r_new*SizeImage]= (byte) 255;
            }
          } else if (r_delta == 0) {
            for ( int j = 0; j < abs(c_delta); j++ ) {
              int c_new =  c_pre  + j*int((abs(c_delta)/(c_delta)));
              int r_new = r_pre;
              nums[c_new+r_new*SizeImage]= (byte) 255;
              
            }
          } else {
            //y=ax+b
            int A =r_delta/c_delta;
            int B = r-A*c;
            for (int j = 0; j < abs(c_delta); j ++) {
              int c_new = c_pre+j*int((abs(c_delta)/c_delta));
              int r_new = A*c_new+B;
              nums[c_new+r_new*SizeImage]= (byte) 255;
            }
          }
        c_pre= c;
        r_pre= r;
        

        
        //nums[c+r*SizeImage]= (byte) 255;
        //int t = c+r*SizeImage;
        //print("size[]:"+t+"\n");
      }
      //copy num
      for(int k = 0 ; k < 196 ; k++){
       nums_[k]=nums[k]; 
      }
              //delitoblooop
        for(int u = 1 ; u < SizeImage - 1 ; u++){//col
         for(int v = 1 ; v < SizeImage - 1 ; v++){//row
          if(nums[u+v*(SizeImage)]==(byte)255){
            nums[(u)+(v-1)*(SizeImage)] = (byte) 127;
            nums[(u-1)+v*(SizeImage)] = (byte) 127;
            nums[(u-1)+(v-1)*(SizeImage)] = (byte) 127;
            
          }
         }
        }
        //OR nums |nums_
       for(int k = 0 ; k < 196 ; k++){
         nums[k]= (byte) (nums[k] | nums_[k]); 
        }
      //String name = "dataset/4/"+id+".dat";
            String name = "dataset/10/0.dat";

      saveBytes(name, nums);
      print("Done write image:"+id);
      id++;
      MaxX = cX;
      MinX = cX;
      MaxY = cY;
      MinY = cY;
      count = 0;
      cA = 0;
    } else {
      print("You draw too short 2\n");
      count = 0;
      cA= 0;
    }
  }
  if (count == 1) {
    background(255); // set background to white
    //translate(0, height);
    stroke(0);
    strokeWeight(1);
    line(0, center, 1000, center);
    line(center, 0, center, 1000);
  }
  if (onIMU) {
    onIMU = false;
    background(255); // set background to white
    //translate(0, height);
    stroke(0);
    strokeWeight(1);
    line(0, center, 1000, center);
    line(center, 0, center, 1000);
  }
  if(count > 10){
  strokeWeight(2);  // Default
  stroke(0, 102, 150);
  h1.update(X, -Y);

  float tmpx = XX+center;
  float tmpy = -YY+center;
  strokeWeight(4);
  stroke(204, 102, 0);
  point(tmpx, tmpy);
  fill(255, 102, 153);
  } else if(count == 1){
    h1.setnew(X, -Y);
  }




  //text("("+str(int(XX))+";"+str(int(YY))+")"+"Dis:"+str(int(Distan))+"rA"+str(int(Angle))+"Apre:"+str(int(AnglePreToShow))+"An:"+str(int(rAngle)), tmpx, tmpy);

  //strokeWeight(7);
  //stroke(204, 0, 200);
  //point(-XXX+center, -YYY+center);
  //h1.update(-XX,-YY);
  //rAngle = rAngle +Angle;
  //XX = XX + Distan*sin(rAngle);
  //XX = YY + Distan*cos(rAngle);
  //noStroke();
  //fill(204,204,204);
  //rect(50,100-20,textWidth(Distance),20);// erase text

  //drawNow(X,Y,2.0);

  // pushMatrix();
  //drawPosNow(XX,YY,2.0);
  //popMatrix();

  //Print values to console
  //print("\t arrayX[count]");
  //if(count > 1)
  //print(arrayX[count-1]);
  //print("\t");
  //print(count);
  //print("\t");
  //print(cX);
  // print("\t");
  //  print(cY);
  //   print("\n");
  //print(X);
  //print("\t
  if (isPrinted) {
    isPrinted =false;
    print(Send);
    println();
  }
}

void serialEvent()
{
  int newLine = 13; // new line character in ASCII
  String message;
  do {
    message = myPort.readStringUntil(newLine); // read from port until new line
    if (message != null) {
      String[] list = split(trim(message), " ");
      if (list.length >= 2 && list[0].equals("Co:")) {
        X = float(list[1]); // convert to float x
        Y = float(list[2]); // convert to float y
        if(count > 10){
        haveData=true;
        cA++;
        cX = X;
        cY = Y;
        if (cX > MaxX ) MaxX = cX;
        if (cX < MinX ) MinX = cX;
        if (cY > MaxY ) MaxY = cY;
        if (cY < MinY ) MinY = cY;
        } else if(count == 1) {
        MaxX = cX;
        MinX = cX;
        MaxY = cY;
        MinY = cY; 
        }
        //Z = float(list[3]); // convert to float z
        count++;
      } else if (list[0].equals("Clean")) {
        onClean = true;
      } 

      if (list.length >= 2 && list[0].equals("Po:")) {
        XXX = float(list[3]); // convert to float x
        YYY = float(list[4]); // convert to float y
        Distan = float(list[5]); // convert to float z
        Angle = float(list[7]); // convert to float z
        rAngle=Angle+AnglePre;
        AnglePreToShow = AnglePre;
        AnglePre = rAngle;
        XX=XXpre+Distan*sin(radians(rAngle));
        YY=YYpre+Distan*cos(radians(rAngle));
        XXpre = XX;
        YYpre = YY;
      } 

      if (list[0].equals("IMUOK")) {
        Send =  message;
        isPrinted = true;
        onIMU = true;
      }
      if (list[0].equals(">>>Send:")) {
        Send =  message;
        isPrinted = true;
      }
      if (list[0].equals("Connected")) {
        Send =  message;
        isPrinted = true;
      }
      if (list[0].equals("Characteristic")) {
        print(">>>Begin<<<");
      }
    }
  } while (message != null);
}

void drawArduino()
{
  /* function contains shape(s) that are rotated with the IMU */
  stroke(0, 90, 90); // set outline colour to darker teal
  fill(0, 130, 130); // set fill colour to lighter teal
  box(300, 10, 200); // draw Arduino board base shape

  stroke(0); // set outline colour to black
  fill(80); // set fill colour to dark grey

  translate(60, -10, 90); // set position to edge of Arduino box
  box(170, 20, 10); // draw pin header as box

  translate(-20, 0, -180); // set position to other edge of Arduino box
  box(210, 20, 10); // draw other pin header as box
}

class HLine { 
  float ypos=0, xpos =0;
  float R, Center;
  HLine (float r, float c  ) {
    R=r;
    Center = c;
  } 
  void update(float x, float y) { 
    line(xpos+Center, ypos+Center, x+Center, y+Center); 
    ypos = y; 
    xpos = x;
  }
    void setnew(float x, float y) { 
    ypos = y; 
    xpos = x;
  }
  
} 
